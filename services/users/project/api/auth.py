# services/users/project/api/auth.py

import jwt
from flask import Blueprint, request
from flask_restful import Api, Resource

from project import bcrypt
from project.api.users.models import User
from project.api.users.services import add_user, get_user_by_email, get_user_by_id

auth_blueprint = Blueprint("auth", __name__)
api = Api(auth_blueprint)


class Register(Resource):
    def post(self):
        post_data = request.get_json()
        response_object = {"status": "fail", "message": "Invalid payload."}
        if not post_data:
            return response_object, 400
        username = post_data.get("username")
        email = post_data.get("email")
        password = post_data.get("password")
        if not username or not email or not password:
            return response_object, 400

        user = get_user_by_email(email)
        if user:
            response_object["message"] = "Sorry. That email already exists."
            return response_object, 400
        user = add_user(username, email, password)
        response_object["status"] = "success"
        response_object["message"] = f"{email} was added!"
        response_object["data"] = {
            "id": user.id,
            "username": user.username,
            "email": user.email,
            "active": user.active,
        }
        return response_object, 201


class Login(Resource):
    def post(self):
        post_data = request.get_json()
        response_object = {"status": "fail", "message": "Invalid payload."}
        if not post_data:
            return response_object, 400

        username = post_data.get("username")
        email = post_data.get("email")
        password = post_data.get("password")

        if not email or not password:
            return response_object, 400

        response_object = {}

        user = get_user_by_email(email)
        if not user or not bcrypt.check_password_hash(user.password, password):
            response_object["message"] = "User does not exist."
            return response_object, 404

        if not username:
            username = user.username

        access_token = user.encode_token(user.id, "access")
        refresh_token = user.encode_token(user.id, "refresh")

        response_object["status"] = "success"
        response_object["message"] = f"User {username} logged."

        response_object["access_token"] = access_token.decode()
        response_object["refresh_token"] = refresh_token.decode()

        return response_object, 200


class Refresh(Resource):
    def post(self):
        post_data = request.get_json()
        response_object = {"status": "fail", "message": "Invalid payload."}
        if not post_data:
            return response_object, 400

        refresh_token = post_data.get("refresh_token")
        if not refresh_token:
            return response_object, 400

        try:
            resp = User.decode_token(refresh_token)
            user = get_user_by_id(resp)
            if not user:
                response_object["message"] = "Invalid token"
                return response_object, 401

            access_token = user.encode_token(user.id, "access")
            refresh_token = user.encode_token(user.id, "refresh")

            response_object["status"] = "success"
            response_object["message"] = "Sucess"
            response_object["access_token"] = access_token.decode()
            response_object["refresh_token"] = refresh_token.decode()

            return response_object, 200

        except jwt.ExpiredSignatureError:
            response_object["message"] = "Signature expired. Please log in again."
            return response_object, 401
        except jwt.InvalidTokenError:
            response_object["message"] = "Invalid token. Please log in again."
            return response_object, 401


class Status(Resource):
    def get(self):
        auth_header = request.headers.get("Authorization")
        response_object = {"status": "fail", "message": "Token required"}

        if not auth_header:
            return response_object, 403

        try:
            access_token = auth_header.split(" ")[1]
            resp = User.decode_token(access_token)
            user = get_user_by_id(resp)
            if not user:
                response_object["message"] = "Invalid token"
                return response_object, 401
            response_object["status"] = "sucess"
            response_object["message"] = "Sucess"
            response_object["data"] = {
                "username": user.username,
                "email": user.email,
                "active": user.active,
            }
            return response_object, 200
        except jwt.ExpiredSignatureError:
            response_object["message"] = "Signature expired. Please log in again."
            return response_object, 401
        except jwt.InvalidTokenError:
            response_object["message"] = "Invalid token. Please log in again."
            return response_object, 401


api.add_resource(Register, "/auth/register")
api.add_resource(Login, "/auth/login")
api.add_resource(Refresh, "/auth/refresh")
api.add_resource(Status, "/auth/status")
